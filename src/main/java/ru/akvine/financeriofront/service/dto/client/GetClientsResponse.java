package ru.akvine.financeriofront.service.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.financeriofront.view.dto.ClientDto;
import ru.akvine.financeriofront.service.Response;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class GetClientsResponse extends Response {
    private List<ClientDto> clients;
}
