package ru.akvine.financeriofront.service;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class Response {
    private Date time;
    private String status;
}
