package ru.akvine.financeriofront.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.akvine.financeriofront.service.dto.client.GetClientsResponse;

@Getter
@Setter
@RequiredArgsConstructor
@Service
public class ClientService {
    private final RestTemplate restTemplate;

    @Value("${backend.server.url}")
    private String backendUrl;
    @Value("${backend.server.port}")
    private String backendPort;

    public GetClientsResponse getClients() {
        String url = backendUrl + backendPort + RestMethods.GET_CLIENTS;
        ResponseEntity<GetClientsResponse> responseEntity =
                restTemplate.getForEntity(url, GetClientsResponse.class);
        return responseEntity.getBody();
    }
}
