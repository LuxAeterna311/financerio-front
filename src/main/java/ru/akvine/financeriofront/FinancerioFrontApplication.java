package ru.akvine.financeriofront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancerioFrontApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinancerioFrontApplication.class, args);
    }

}
