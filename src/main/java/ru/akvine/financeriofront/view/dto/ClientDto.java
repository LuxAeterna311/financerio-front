package ru.akvine.financeriofront.view.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class ClientDto {
    private String uuid;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String phone;
    private String email;
    private Date createdDate;
    private Date updatedDate;
    private boolean deleted;
}