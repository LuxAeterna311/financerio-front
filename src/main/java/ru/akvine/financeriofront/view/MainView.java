package ru.akvine.financeriofront.view;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.akvine.financeriofront.service.ClientService;
import ru.akvine.financeriofront.view.dto.ClientDto;

@Route("/clients")
public class MainView extends VerticalLayout {
    private ClientService clientService;
    Grid<ClientDto> clientGrid = new Grid<>(ClientDto.class);

    public MainView(@Autowired ClientService clientService) {
        init(clientService);
        configure();

        add(clientGrid);
        updateList();
    }

    private void init(ClientService clientService) {
        this.clientService = clientService;
    }

    private void configure() {
        addClassName("list-view");
        setSizeFull();
        clientGrid.setColumns("firstName", "secondName", "thirdName", "phone", "email");
    }


    private void updateList() {
        clientGrid.setItems(clientService
                .getClients()
                .getClients());
    }
}
